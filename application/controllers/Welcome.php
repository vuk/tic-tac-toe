<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use TTT\Model\User;
use TTT\Model\Match;

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	    $user = new User();
	    $user->first_name = 'Vuk';
	    $user->last_name = 'Stankovic';
	    $user->save();
		$this->load->view('master');

		$match = new Match();
		$match->player_1 = $user->id;
		$match->player_2 = $user->id;
		$match->winner = $user->id;
		$match->save();
	}
}
