<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: vuk
 * Date: 11.2.17.
 * Time: 00.09
 */
use Illuminate\Database\Capsule\Manager as Capsule;

$config['capsule'] = new Capsule;

$config['capsule']->addConnection(array(
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'ticTacToe',
    'username'  => 'root',
    'password'  => 'root',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => 'ttt_'
));

// Make this Capsule instance available globally via static methods... (optional)
$config['capsule']->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$config['capsule']->bootEloquent();