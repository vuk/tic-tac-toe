<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<?= base_url() ?>bower_components/bootstrap/dist/css/bootstrap.css">
</head>
<body ng-app="ngTicTacToe">
    <h1>Testing initial setup</h1>

    <script src="<?= base_url() ?>bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?= base_url() ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>bower_components/angular/angular.js"></script>
</body>
</html>