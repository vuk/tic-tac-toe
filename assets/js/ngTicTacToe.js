'use strict';
(function () {

    angular.module('ngTicTacToe.services', []);
    angular.module('ngTicTacToe.directives', []);
    angular.module('ngTicTacToe.controllers', []);
    angular.module('ngTicTacToe', [
        'ngTicTacToe.services',
        'ngTicTacToe.directives',
        'ngTicTacToe.controllers'
    ]);

})();